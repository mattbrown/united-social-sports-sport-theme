<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
  wp_enqueue_script("jquery");
	wp_head();
?>
        <script type="text/javascript" language="javascript">
        jQuery(document).ready(function($){
          
           $('.inplay-tap').first().addClass('div-curr-tab');
           $('.inplay-tap').click(function(){
                IDD=$(this).attr('id');
                $('#block_curr_s').val(IDD);
                $('.division-content').hide();                  
                $('#block_'+IDD).show();
                $('.division-info').hide();
                $('#block_'+IDD+' .division-info').first().show();  
                $('.inplay-tap').removeClass('div-curr-tab');
                
                $(this).addClass('div-curr-tab');
                return false;
           });

           $('.div-post-item').live('click',function(){

            $(this).addClass('blockactive');
            ID=this.id;
            curr=$('#block_curr').val();
            tab=$('#block_curr_s').val();
            
            if(ID){
                $('.division-info').hide();
                //$('#'+tab+'_rightblock_'+curr).hide();
                $('#'+tab+'_rightblock_'+ID).fadeIn('slow');
                $('#block_curr').val(ID);
            }
            return false;
           })   
        });
    </script>

    
    <script type="text/javascript" src="<?php bloginfo('template_url');?>/font/cufon-yui.js"></script>
   

</head>

<?php $theme_options = get_option('socialsports_theme_options'); ?>
<body <?php body_class(); ?>>
<div id="container">
    <div id="header" class="main">
        <div class="header-logo">&nbsp;<a href="<?php echo site_url()?>"><img src="<?php echo esc_url($theme_options['header_logo']) ?>" /></a></div>
        
        <?php dynamic_sidebar('weather-hotline')?>
        
        <div class="header-login">
            <form name="login_top" method="POST" action="http://commi.sh/session">
                <input type="text" name="login" value="Username" onFocus="if(this.value == 'Username') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Username';}"/>                
                <input type="text" name="password" value="Password"  onFocus="if(this.value == 'Password') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Password';}"/>
                <input type="image" src="<?php bloginfo('template_url');?>/images/login_btn.png" />
            </form>
        </div>
        
        <?php dynamic_sidebar('header-social-sports')?>
        
        <div class="clear"></div>
    </div><!--end #header-->
    <div class="clear"></div>
    
    <div id="access" class="main">
        <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary','menu'=>'Main Menu', 'menu_class' => 'main-menu' ) ); ?>
        
    </div><!--end #header-nav-->
    <div class="clear"></div>
    
    <div id="slide">
         <img src="<?php header_image(); ?>" alt="" />
    </div>
