<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="content">
        <div id="main-content" class="main-content-page">
            <div class="header-page-content">
                <a href="#">Home</a> <img src="<?php bloginfo('template_url')?>/images/arrow_page.jpg" /> <a href="#">Page</a>
            </div>
            <div class="page-content">

				<h1 class="page-title"><?php
					printf( __( 'Category Archives: %s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' );
				?></h1>
				<?php
					$category_description = category_description();
					if ( ! empty( $category_description ) )
						echo '<div class="archive-meta">' . $category_description . '</div>';

				/* Run the loop for the category page to output the posts.
				 * If you want to overload this in a child theme then include a file
				 * called loop-category.php and that will be used instead.
				 */
				get_template_part( 'loop', 'category' );
				?>

			</div>
            <div class="footer-page-content">
            
            </div>
        </div><!--end #main-content-->
        
        <?php get_sidebar()?>
        
    </div><!--end #content-->

<?php get_footer(); ?>
