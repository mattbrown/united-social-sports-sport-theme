var counter = 0;
var current_image_url = '';
var current_image = '';
var current_buttons = '';

jQuery(document).ready(function(){
  counter = jQuery("input[name=image_count]").val();
  jQuery("#add_sponsor_image").click(function(){
    var imageRow = jQuery("#sponsor_row_placeholder").clone();
    var newId = "sponsor_row_" + counter;
    imageRow.attr("id", newId);
    imageRow.show();
    
    var imageField = jQuery("#sponsor_image", imageRow);  
    imageField.attr("id", "sponsor_image_" + counter); 
    
    var imageUrlField = jQuery("#sponsor_image_url", imageRow);  
    imageUrlField.attr("id", "sponsor_image_url_" + counter); 
    imageUrlField.attr("name", "socialsports_theme_options[sponsors][" + counter + "][image_url]");
    
    var uploadButton = jQuery("#upload_button", imageRow);
    uploadButton.attr("id", "upload_button_" + counter);
    uploadButton.click(
      (function(i){
        return function(){
          current_image_url = jQuery('#sponsor_image_url_' + i);
          current_image = jQuery('#sponsor_image_' + i);
          current_buttons = current_image_url.next('#image_buttons');
          
          tb_show('Upload a sponsor image', 'media-upload.php?referer=socialsports_theme_options&type=image&TB_iframe=true&post_id=0', false);
          return false;
        };
      })(counter)
    );
    
    var chooseFromLibraryButton = jQuery("#choose_from_library_button", imageRow);
    chooseFromLibraryButton.attr("id", "choose_from_library_button_" + counter);
    chooseFromLibraryButton.click(
      (function(i){
        return function(){
          current_image_url = jQuery('#sponsor_image_url_' + i);
          current_image = jQuery('#sponsor_image_' + i);
          current_buttons = current_image_url.next('#image_buttons');
          
          tb_show('Choose sponsor image.', 'media-upload.php?referer=socialsports_theme_options&tab=library&type=image&TB_iframe=true&post_id=0', false);
          return false;
        };
      })(counter)
    );
    
    var urlField = jQuery("#sponsor_url", imageRow);  
    urlField.attr("id", "sponsor_url_" + counter); 
    urlField.attr("name", "socialsports_theme_options[sponsors][" + counter + "][url]");
    
    var removeLink = jQuery("a", imageRow).click(function(){
      removeImage(imageRow);
      return false;
    });
    
    counter++;
    
    jQuery("input[name=image_count]").val(counter);
    jQuery("#sponsor_images_list").append(imageRow);
    return false;
  });
  
  jQuery("#sponsor_images_list li").each(function(index,imageRow){
    jQuery("a", this).click(function(){
      removeImage(imageRow);
      return false;
    });
  });
      
  jQuery("#sponsor_images_list").sortable({
    stop: function(event, ui){
      var i = 0;
      jQuery("li", this).each(function(){
        setRowId(this, i);
        i++;
      });
      counter = i;
      jQuery("input[name=image_count]").val(counter);
    }
  });
  
  // =============================
  // = Header and footer buttons =
  // =============================
  jQuery('#upload_header_logo').click(function(){
    current_image_url = jQuery('#header_logo');
    current_image = jQuery('#header_logo_image');
    current_buttons = '';
          
    tb_show('Upload a header logo', 'media-upload.php?type=image&TB_iframe=true&post_id=0', false);
    return false;
  });
  
  jQuery('#choose_header_logo').click(function(){
    current_image_url = jQuery('#header_logo');
    current_image = jQuery('#header_logo_image');
    current_buttons = '';
          
    tb_show('Upload a header logo', 'media-upload.php?tab=library&type=image&TB_iframe=true&post_id=0', false);
    return false;
  });
  
  jQuery('#upload_footer_logo').click(function(){
    current_image_url = jQuery('#footer_logo');
    current_image = jQuery('#footer_logo_image');
    current_buttons = '';
          
    tb_show('Upload a footer logo', 'media-upload.php?type=image&TB_iframe=true&post_id=0', false);
    return false;
  });
  
  jQuery('#choose_footer_logo').click(function(){
    current_image_url = jQuery('#footer_logo');
    current_image = jQuery('#footer_logo_image');
    current_buttons = '';
          
    tb_show('Upload a footer logo', 'media-upload.php?tab=library&type=image&TB_iframe=true&post_id=0', false);
    return false;
  });
  
  // =====================
  // = Remove logo links =
  // =====================
  jQuery('#remove_header_logo').click(function(){
    jQuery('#header_logo_image').replaceWith('<img id="header_logo_image" />');
    jQuery('#header_logo').val('');
  });
  
  jQuery('#remove_footer_logo').click(function(){
    jQuery('#footer_logo_image').replaceWith('<img id="footer_logo_image" />');
    jQuery('#footer_logo').val('');
  });
  
  // ================================================
  // = Set Media uploader to update images and urls =
  // ================================================
  window.send_to_editor = function(html){
    var image_url = jQuery('img',html).attr('src');
    current_image_url.val(image_url);
    tb_remove();
      
    current_image.attr('src',image_url);
    current_buttons.remove();
  }
});

function removeImage(image){
  jQuery(image).remove();
  var i = 0;
  jQuery("#sponsor_images_list li").each(function(){
    setRowId(this, i);
    i++;
  });
  counter = i;
  jQuery("input[name=image_count]").val(counter);
}

function setRowId(row, id){
  var newId = "sponsor_row_" + id;
  jQuery(row).attr("id", newId);
  
  var imageField = jQuery("#sponsor_image", row);  
  imageField.attr("id", "sponsor_image_" + id);
  
  var imageUrlField = jQuery("input[id^='sponsor_image_url']", row);  
  imageUrlField.attr("id", "sponsor_image_url_" + id); 
  imageUrlField.attr("name", "socialsports_theme_options[sponsors][" + id + "][image_url]");
  
  var uploadButton = jQuery("input[id^='upload_button']", row);
  uploadButton.attr("id", "upload_button_" + id);
  
  var chooseFromLibraryButton = jQuery("input[id^='choose_from_library_button']", row);
  chooseFromLibraryButton.attr("id", "choose_from_library_button_" + id);
  
  var urlField = jQuery("input[id^='sponsor_url']", row);  
  urlField.attr("id", "sponsor_url_" + id); 
  urlField.attr("name", "socialsports_theme_options[sponsors][" + id + "][url]");
}