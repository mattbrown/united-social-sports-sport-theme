<?php
/**
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    <div id="division-box">
        <div class="division-top">
            <div class="division-header">
                <span class="division-font1">DCKickball -  </span> <span class="division-font2"> Kicking Balls and Flipping Cups</span>
            </div>
            <div class="division-nav">
                <ul>
                </ul>
            </div>
            <div class="clear"></div>
        </div>        

<div class="division-content" id="block_inplay" style="">
            <div class="division-menu">
            </div>
            <div class="division-info" id="inplay__rightblock_" style="">
                <h3><?the_title()?></h3>
                <div class="division-info-add">
                    <p class="lbl-division-info">Days:</p><p class="txt-division-info">
                    </p><div class="clear"></div>
                    <p class="lbl-division-info">Level:</p><p class="txt-division-info"></p><div class="clear"></div>
                    <p class="lbl-division-info">Location:</p><p class="txt-division-info"></p><div class="clear"></div>
                </div>
                <p class="division-info-text"><?the_excerpt()?></p>
                <div class="sign-up-detail">
                    <p>Full Details & Sign up here, click on a day:</p>
                    <div class="clear"></div>
                </div>
            </div>            
            <input id="block_curr" value="" type="hidden" />   
            <input id="block_curr_s" value="" type="hidden" />         
</div>    
   

        <div class="clear"></div>
    </div>
    
    <div id="content">
        <div id="main-content">
            
                <?php
                    global $post;
                    $args = array( 'numberposts' => 2, 'category' => 5, 'order'=> 'DESC', 'orderby' => 'date' );
                    $myposts = get_posts( $args );
                    foreach( $myposts as $post ) :	setup_postdata($post); ?>
                        <div class="main-content-box">        	
                            <div class="txtmain-content-box">
                                <h2><a href="<?php the_permalink()?>"><?php the_title()?></a></h2>
                                <span class="author-post"><?php twentyten_posted_on();?></span>
                                <div class="home-img-box"><?php the_post_thumbnail()?></div>                        
                                <?php the_content()?>
                            </div>
                            
                            <div class="footer-page-content">
                                <span class="comment-number"><?php comments_number('0'); ?> Comments</span>
                                <?php dynamic_sidebar('tweet-and-like')?>
                                <div class="clear"></div> 
                            </div>
                            
                        </div>
                    <?php endforeach; ?>
                
            
        </div><!--end #main-content-->
        
        <?php get_sidebar()?>
        
    </div><!--end #content-->
		
<?php get_footer(); ?>
