<?php
/**
 * Add widgets when a blog is created
 */
 
function add_default_widgets($blog_id)
{
  $sidebars = array(
    'right_sidebar' => array(
      'social_media_icon_widget',
      'sponsors_widget',
      'commish_registrations_widget',
      'facebook_widget',
      'twitter_widget',
    ),
    'weather-hotline' => array(
      'hotline_widget',
    ),
    'header-social-sports' => array(
      'uss_logo_widget',
    ),
  );
  
  foreach($sidebars as $sidebar => $widgets){
    foreach($widgets as $widget_id){
      update_option('widget_' . $widget_id, array());
    }
  }
  update_option('sidebars_widgets', $sidebars);
  return;
} // end add_default_widgets
add_action('admin_init', 'add_default_widgets');

// ================
// = Social Media =
// ================
wp_register_sidebar_widget(
  'social_media_icon_widget',
  'Social Media Icon',
  'social_media_icon_widget_display',
  array(
    'description' => 'Displays icons for social networks.'
  )
);

wp_register_widget_control(
  'social_media_icon_widget',
  'social_media_icon_widget',
  'social_media_icon_widget_control'
);

function social_media_icon_widget_control($args=array(), $params=array()){
  echo "To change settings, please use the Theme Options panel.";
}

function social_media_icon_widget_display($args=array(), $params=array()){
  $options = get_option('socialsports_theme_options');
  ?>
  <div class="clear"></div>
  <div class="social-media-box">
    <?php if(!empty($options['facebook_username'])): ?>
      <a href="http://facebook.com/<?php esc_attr_e($options['facebook_username']); ?>">
        <img src="<?php echo get_template_directory_uri() ?>/images/f_icon.png" />
      </a>
    <?php endif; ?>
  
    <?php if(!empty($options['twitter_username'])): ?>
      <a href="http://twitter.com/<?php esc_attr_e($options['twitter_username']); ?>">
        <img src="<?php echo get_template_directory_uri() ?>/images/t_icon.png" />
      </a>
    <?php endif; ?>
  
    <?php if(!empty($options['meetup_username'])): ?>
      <a href="http://meetup.com/<?php esc_attr_e($options['meetup_username']); ?>">
        <img src="<?php echo get_template_directory_uri() ?>/images/m_icon.png" />
      </a>
    <?php endif; ?>
  
  
    <a href="<?php bloginfo('rss2_url') ?>">
      <img src="<?php echo get_template_directory_uri() ?>/images/r_icon.png" />
    </a>
  </div>
  <?php  
}

// ================
// = Social Media =
// ================
wp_register_sidebar_widget(
  'hotline_widget',
  'Hotline',
  'hotline_widget_display',
  array(
    'description' => 'Displays phone number.'
  )
);

wp_register_widget_control(
  'hotline_widget',
  'hotline_widget',
  'hotline_widget_control'
);

function hotline_widget_control($args=array(), $params=array()){
  echo "To change settings, please use the Theme Options panel.";
}

function hotline_widget_display($args=array(), $params=array()){
  $options = get_option('socialsports_theme_options');
  ?>
    <div class="header-info">
      <h3><?php echo esc_attr_e($options['hotline_title']) ?></h3>
      <div><?php echo esc_attr_e($options['hotline_text']) ?></div>
    </div>
  <?php  
}

// =======================
// = USS logo widget     =
// =======================
wp_register_sidebar_widget(
  'uss_logo_widget',
  'United Social Sports Logo',
  'uss_logo_widget_display',
  array(
    'description' => 'Displays the United Social Sports logo.'
  )
);

wp_register_widget_control(
  'uss_logo_widget',
  'uss_logo_widget',
  'uss_logo_widget_control'
);

function uss_logo_widget_control($args=array(), $params=array()){
  echo "To change settings, please use the Theme Options panel.";
}

function uss_logo_widget_display($args=array(), $params=array()){
  $options = get_option('socialsports_theme_options');
  if($options['show_uss_logo']){
    echo $args['before_widget'];
    ?>
      <a href="http://unitedsocialsports.com/" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/images/social_sports.png"></a>
    <?php
    echo $args['after_widget'];
  }
}

// ===========
// = Facebook =
// ===========
wp_register_sidebar_widget(
  'facebook_widget',
  'Facebook Widget',
  'facebook_widget_display',
  array(
    'description' => 'Displays facebook widget.'
  )
);

wp_register_widget_control(
  'facebook_widget',
  'facebook_widget',
  'facebook_widget_control'
);

function facebook_widget_control($args=array(), $params=array()){
  echo "To change settings, please use the Theme Options panel.";
}

function facebook_widget_display($args=array(), $params=array()){
  $options = get_option('socialsports_theme_options');
  if(!empty($options['facebook_username'])){
    ?>
    <div class="clear"></div>
    <div class="dckickball-face-header2">

      <div id="fb-root"></div>
      <script>
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) {return;}
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=113198335404256";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
      </script>
      <div class="fb-like-box" data-href="http://facebook.com/<?php echo $options['facebook_username'] ?>" data-width="300" data-show-faces="true" data-stream="false" data-header="true"></div>
    </div>
    <div style="clear: both;"></div>
    <div style="padding-bottom: 10px;"></div>
    <?php
  }
}

// =======================
// = Tweet and like post =
// =======================
wp_register_sidebar_widget(
  'tweet_and_like_widget',
  'Tweet and Like Widget',
  'tweet_and_like_widget_display',
  array(
    'description' => 'Displays Tweet and Like widget.'
  )
);

wp_register_widget_control(
  'tweet_and_like_widget',
  'tweet_and_like_widget',
  'tweet_and_like_widget_control'
);

function tweet_and_like_widget_control($args=array(), $params=array()){
  echo "To change settings, please use the Theme Options panel.";
}

function tweet_and_like_widget_display($args=array(), $params=array()){
  ?>
  [tweetandlike_buttons]
  <?php
}

// =======================
// = Sponsors widget     =
// =======================
wp_register_sidebar_widget(
  'sponsors_widget',
  'Sponsors Widget',
  'sponsors_widget_display',
  array(
    'description' => 'Displays sponsor images.'
  )
);

wp_register_widget_control(
  'sponsors_widget',
  'sponsors_widget',
  'sponsors_widget_control'
);

function sponsors_widget_control($args=array(), $params=array()){
  echo "To change settings, please use the Theme Options panel.";
}

function sponsors_widget_display($args=array(), $params=array()){
  $options = get_option('socialsports_theme_options');
  $sponsors = $options['sponsors'];
  
  if( ! empty($sponsors)){
    ?>
    <div class="sidebar-box proudly-sponsored">
      <h3><?php echo esc_attr_e($options['sponsors_title']) ?></h3>
      <div style="width: 250px;margin:0 auto;height:215px">
        <div id="proudly-sponsored" >
          <?php foreach($sponsors as $sponsor): ?>
            <a href="<?php echo esc_url($sponsor['url']) ?>"><img src="<?php echo esc_url($sponsor['image_url']) ?>" /></a>
          <?php endforeach; ?>
        </div>
        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jquery.cycle.min.js"></script>
        <script type='text/javascript'>
          jQuery(document).ready(function() {
            jQuery('#proudly-sponsored').cycle({
  		        fx: 'fade' 
            });
          });
        </script>
        <div style="clear: both;"></div>
      </div>
    </div>
    <?php
  }
}

// =========================
// = Commish registrations =
// =========================
wp_register_sidebar_widget(
  'commish_registrations_widget',
  'Commish Registrations Widget',
  'commish_registrations_widget_display',
  array(
    'description' => 'Shows recent registrations from Commish.'
  )
);

wp_register_widget_control(
  'commish_registrations_widget',
  'commish_registrations_widget',
  'commish_registrations_widget_control'
);

function commish_registrations_widget_control($args=array(), $params=array()){
  echo "To change settings, please use the Theme Options panel.";
}

function commish_registrations_widget_display($args=array(), $params=array()){
  $options = get_option('socialsports_theme_options');
  if( ! empty($options['commish_league_id'])){
    ?>
      <div class="sidebar-box proudly-sponsored">
        <h3>Recent Registrations</h3>
        <script src="http://commi.sh/registrations/scroller.js?league_id=<?php echo esc_attr_e($options['commish_league_id']) ?>" type="text/javascript"></script>
      </div>
    <?php
  }
}

// ===========
// = Twitter =
// ===========
wp_register_sidebar_widget(
  'twitter_widget',
  'Twitter Widget',
  'twitter_widget_display',
  array(
    'description' => 'Displays latest tweets.'
  )
);

wp_register_widget_control(
  'twitter_widget',
  'twitter_widget',
  'twitter_widget_control'
);

function twitter_widget_control($args=array(), $params=array()){
  echo "To change settings, please use the Theme Options panel.";
}

function twitter_widget_display($args=array(), $params=array()){
  $options = get_option('socialsports_theme_options');
  if(!empty($options['twitter_username'])){
    ?>
    <div class="clear"></div>
    <script charset="utf-8" src="http://widgets.twimg.com/j/2/widget.js"></script>
    <script>
    new TWTR.Widget({
     version: 2,
     type: 'profile',
     rpp: 2,
     interval: 30000,
     width: 300,
     height: 300,
     theme: {
       shell: {
         background: '#7a7a78',
         color: '#ffffff'
       },
       tweets: {
         background: '#ffffff',
         color: '#0d0c0d',
         links: '#ba0633'
       }
     },
     features: {
       scrollbar: false,
       loop: false,
       live: false,
       behavior: 'all'
     }
    }).render().setUser('<?php echo $options['twitter_username']; ?>').start();
    </script>
    <?php
  }
}