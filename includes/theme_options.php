<?php
/* ------------------------------------------------------------------------ *
 * Initialize theme options
 * ------------------------------------------------------------------------ */
function socialsports_theme_options_init(){  
  $socialsports_theme_options = get_option('socialsports_theme_options');
  if(false === $socialsports_theme_options){ //the options aren't in the db
    $socialsports_theme_options = socialsports_get_default_options();
    add_option('socialsports_theme_options', $socialsports_theme_options);
  }
  
  //Change media uploader text
  global $pagenow;
  if('media-upload.php' == $pagenow || 'async-upload.php' == $pagenow){
    add_filter('gettext', 'replace_thickbox_text', 1, 3);
  }
}
add_action('admin_init', 'socialsports_theme_options_init');

function replace_thickbox_text($translated_text, $text, $domain){
  if('Insert into Post' == $text){
    $referer = strpos(wp_get_referer(), 'socialsports_theme_options');
    if('' != $referer){
      return __('Add this sponsor image.', 'socialsports');
    }
  }
  
  return $translated_text;
}

function socialsports_get_default_options()
{
  return array(
    'header_logo' => '',
    'footer_logo' => '',
    'hotline_title' => 'Weather Hotline:',
    'hotline_text' => '1-000-000-0000',
    'commish_league_id' => '',
    'show_uss_logo' => true,
    'twitter_username' => '',
    'facebook_username' => '',
    'meetup_username' => '',
    'sponsors' => array(),
    'sponsors_title' => 'Proudly sponsored by',
  );
} // end socialsports_get_default_options

function socialsports_theme_options_enqueue_scripts()
{
  wp_register_script('socialsports-upload', get_template_directory_uri() . '/js/socialsports-upload.js', array('jquery','media-upload','thickbox'));
  if('appearance_page_socialsports_theme_options' == get_current_screen()->id){
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-sortable');
    wp_enqueue_script('thickbox');
    wp_enqueue_style('thickbox');
    wp_enqueue_script('media-upload');
    wp_enqueue_script('socialsports-upload');
  }
} // end socialsports_theme_options_enqueue_scripts
add_action('admin_enqueue_scripts', 'socialsports_theme_options_enqueue_scripts');

/* ------------------------------------------------------------------------ *
 * Theme options menu initialization
 * ------------------------------------------------------------------------ */
function socialsports_create_theme_menu(){
  add_theme_page(
    __('Social Sports Theme Options', 'socialsports'), 
    __('Theme Options', 'socialsports'), 
    'edit_theme_options', 
    'socialsports_theme_options', 
    'socialsports_theme_options_display'
  );
}
add_action('admin_menu', 'socialsports_create_theme_menu');


/* ------------------------------------------------------------------------ *
 * Setting page display
 * ------------------------------------------------------------------------ */
function socialsports_theme_options_display(){
?>
  <div class="wrap">
    <div id="icon-themes" class="icon32"></div>
    <h2><?php _e('Social Sports Theme Options', 'socialsports') ?></h2>
    <?php settings_errors(); ?>
    <form method="post" action="options.php" enctype="multipart/form-data">
      <?php settings_fields('socialsports_theme_options'); ?>
      <?php do_settings_sections('socialsports_theme_options'); ?>
      
      <?php submit_button(); ?>
      <p><input type="submit" name="socialsports_theme_options[reset]" class="button-secondary" value="<?php esc_attr_e('Reset Defaults', 'socialsports') ?>"></p>   
    </form>
  </div><!-- /.wrap -->
<?php
} // end socialsports_theme_options_display

/* ------------------------------------------------------------------------ *
 * Setting Registration
 * ------------------------------------------------------------------------ */
add_action('admin_init', 'socialsports_register_options');
function socialsports_register_options() {
  // ===================
  // = General options =
  // ===================
  
  add_settings_section(
    'general_settings_section',
    __('General Options', 'socialsports'),
    'socialsports_general_options_callback',
    'socialsports_theme_options'
  );
  
  add_settings_field(
    'header_logo',
    __('Header logo', 'socialsports'),
    'socialsports_header_logo_callback',
    'socialsports_theme_options',
    'general_settings_section'
  );
  
  add_settings_field(
    'footer_logo',
    __('Footer logo', 'socialsports'),
    'socialsports_footer_logo_callback',
    'socialsports_theme_options',
    'general_settings_section'
  );
  
  add_settings_field(
    'hotline_title',
    __('Header hotline title', 'socialsports'),
    'socialsports_hotline_title_callback',
    'socialsports_theme_options',
    'general_settings_section'
  );
  
  add_settings_field(
    'hotline_text',
    __('Header hotline text', 'socialsports'),
    'socialsports_hotline_text_callback',
    'socialsports_theme_options',
    'general_settings_section'
  );
  
  add_settings_field(
    'show_uss_logo',
    __('Show United Social Sports logo', 'socialsports'),
    'socialsports_toggle_uss_logo_callback',
    'socialsports_theme_options',
    'general_settings_section'
  );
  
  add_settings_field(
    'commish_league_id',
    __('Commish League ID', 'socialsports'),
    'socialsports_commish_league_id_callback',
    'socialsports_theme_options',
    'general_settings_section'
  );
  
  // ==================
  // = Social options =
  // ==================
  add_settings_section(
    'social_settings_section',
    __('Social Media Options', 'socialsports'),
    'socialsports_social_options_callback',
    'socialsports_theme_options'
  );

  add_settings_field(
    'twitter_username',
    __('Twitter', 'socialsports'),
    'socialsports_twitter_callback',
    'socialsports_theme_options',
    'social_settings_section'
  );

  add_settings_field(
    'facebook_username',
    __('Facebook', 'socialsports'),
    'socialsports_facebook_callback',
    'socialsports_theme_options',
    'social_settings_section'
  );
  add_settings_field(
    'meetup_username',
    __('Meetup', 'socialsports'),
    'socialsports_meetup_callback',
    'socialsports_theme_options',
    'social_settings_section'
  );
  
  // ===================
  // = Sponsor options =
  // ===================
  
  add_settings_section(
    'sponsor_settings_section',
    __('Sponsor Options', 'socialsports'),
    'socialsports_sponsor_options_callback',
    'socialsports_theme_options'
  );
  
  add_settings_field(
  'sponsors_title',
    __('Sponsors Title', 'socialsports'),
    'socialsports_sponsors_title_callback',
    'socialsports_theme_options',
    'sponsor_settings_section'
  );
  
  add_settings_field(
  'sponsors',
    __('Sponsors', 'socialsports'),
    'socialsports_sponsors_callback',
    'socialsports_theme_options',
    'sponsor_settings_section'
  );

  // =====================
  // = Register settings =
  // =====================
  register_setting(
    'socialsports_theme_options',
    'socialsports_theme_options',
    'socialsports_options_sanitize_and_validate'
  );
} // end sandbox_initialize_theme_options

/* ------------------------------------------------------------------------ *
 * Section Callbacks
 * ------------------------------------------------------------------------ */
function socialsports_general_options_callback() {
 echo '<p>Leave options blank to disable.</p>';
} // end socialsports_general_options_callback
 
function socialsports_social_options_callback() {
  echo '<p>Provide the information for the social networks you\'d like to display.</p>';
} // end socialsports_social_options_callback

function socialsports_sponsor_options_callback()
{
  echo '<p>Manage sponsors. Drag to sort.</p>';
} // end socialsports_sponsor_options_callback

/* ------------------------------------------------------------------------ *
 * Field Callbacks
 * ------------------------------------------------------------------------ */
function socialsports_header_logo_callback()
{
  $options = get_option('socialsports_theme_options');
  ?>
    <p>
      <img id="header_logo_image" src="<?php echo esc_url($options['header_logo']) ?>" />
      <input type="hidden" id="header_logo" name="socialsports_theme_options[header_logo]" value="<?php echo esc_url($options['header_logo']) ?>" />
      <div id="image_buttons">
        <input id="upload_header_logo" type="button" class="button" value="<?php _e('Upload Image', 'socialsports') ?>" />
        <span>&nbsp;or&nbsp;</span>
        <input id="choose_header_logo" type="button" class="button" value="<?php _e('Choose Image from Library', 'socialsports') ?>" />
      </div>
      <p><a id="remove_header_logo" href="#" style="color:red;">Remove logo</a></p>
    </p>
  <?php
} // end socialsports_header_logo_callback

function socialsports_footer_logo_callback()
{
  $options = get_option('socialsports_theme_options');
  ?>
    <p>
      <img id="footer_logo_image" src="<?php echo esc_url($options['footer_logo']) ?>" />
      <input type="hidden" id="footer_logo" name="socialsports_theme_options[footer_logo]" value="<?php echo esc_url($options['footer_logo']) ?>" />
      <div id="image_buttons">
        <input id="upload_footer_logo" type="button" class="button" value="<?php _e('Upload Image', 'socialsports') ?>" />
        <span>&nbsp;or&nbsp;</span>
        <input id="choose_footer_logo" type="button" class="button" value="<?php _e('Choose Image from Library', 'socialsports') ?>" />
      </div>
      <p><a id="remove_footer_logo" href="#" style="color:red;">Remove logo</a></p>
    </p>
  <?php
} // end socialsports_footer_logo_callback

function socialsports_hotline_title_callback()
{
 $options = get_option('socialsports_theme_options');
 ?>
   <input type="text" id="hotline_title" name="socialsports_theme_options[hotline_title]" value="<?php echo $options['hotline_title'] ?>" />
 <?php
} // end socialsports_hotline_title_callback
 
function socialsports_hotline_text_callback()
{
 $options = get_option('socialsports_theme_options');
 ?>
   <input type="text" id="hotline_text" name="socialsports_theme_options[hotline_text]" value="<?php echo $options['hotline_text'] ?>" />
 <?php
} // end socialsports_hotline_text_callback

function socialsports_toggle_uss_logo_callback()
{
  $options = get_option('socialsports_theme_options');
  ?>
    <input type="checkbox" id="show_uss_logo" name="socialsports_theme_options[show_uss_logo]" value="1" <?php echo checked(1, $options['show_uss_logo'], false) ?> />
  <?php
} // end socialsports_toggle_uss_logo_callback

function socialsports_commish_league_id_callback()
{
  $options = get_option('socialsports_theme_options');
  ?>
    <input type="text" id="commish_league_id" name="socialsports_theme_options[commish_league_id]" value="<?php echo esc_attr_e($options['commish_league_id']) ?>" size="5" />
    <span class="description"><?php _e('Obtained from Commish account.', 'socialsports'); ?></span>
  <?php
} // end socialsports_commish_league_id_callback
 
function socialsports_twitter_callback() {  
  $options = get_option('socialsports_theme_options');
  ?> 
    <input type="text" id="twitter_username" name="socialsports_theme_options[twitter_username]" value="<?php echo esc_attr_e($options['twitter_username']) ?>" />
    <span class="description"><?php _e('e.g. yourusername', 'socialsports'); ?></span>
  <?php
} // end socialsports_twitter_callback 

function socialsports_facebook_callback() {  
  $options = get_option('socialsports_theme_options');  
  ?> 
    <input type="text" id="facebook_username" name="socialsports_theme_options[facebook_username]" value="<?php echo esc_attr_e($options['facebook_username']) ?>" />
    <span class="description"><?php _e('e.g. yourusername', 'socialsports'); ?></span>
  <?php
} // end socialsports_facebook_callback  
function socialsports_meetup_callback() {  
  $options = get_option('socialsports_theme_options');  
  ?> 
    <input type="text" id="meetup_username" name="socialsports_theme_options[meetup_username]" value="<?php echo esc_attr_e($options['meetup_username']) ?>" />
    <span class="description"><?php _e('e.g. yourusername', 'socialsports'); ?></span>
  <?php
} // end socialsports_meetup_callback

function socialsports_sponsors_title_callback()
{
  $options = get_option('socialsports_theme_options');
  ?>
    <input type="text" id="sponsors_title" name="socialsports_theme_options[sponsors_title]" value="<?php echo esc_attr_e($options['sponsors_title']) ?>" />
    <span class="description"><?php _e('Appears above sponsor images', 'socialsports'); ?></span>
  <?php
} // end socialsports_sponsor_title_callback

function socialsports_sponsors_callback()
{
  $options = get_option('socialsports_theme_options');
  $sponsors = $options['sponsors'];
  ?>
  <ul id="sponsor_images_list">
    <?php $i = 0; ?>
    <?php foreach($sponsors as $sponsor): ?>
      <li class="sponsor_image" id="sponsor_row_<?php echo $i ?>">
        <p>
          <label><?php _e('Sponsor URL:', 'socialsports') ?></label>
          <input type="text" id="sponsor_url_<?php echo $i ?>" name="socialsports_theme_options[sponsors][<?php echo $i ?>][url]" value="<?php echo esc_url($sponsor['url']) ?>">
        </p>
        <p>
          <img id="sponsor_image_<?php echo $i ?>" src="<?php echo esc_url($sponsor['image_url']) ?>" />
          <input type="hidden" id="sponsor_image_url_<?php echo $i ?>" name="socialsports_theme_options[sponsors][<?php echo $i ?>][image_url]" value="<?php echo esc_url($sponsor['image_url']) ?>" />
        </p>
        <p><a href="#" class="button"><?php _e('Remove Sponsor','socialsports') ?></a></p>
        <br />
      </li>
      <?php $i++; ?>
    <?php endforeach; ?>
    </ul>
    <input type="hidden" name="image_count" value="<?php echo count($sponsors) ?>"/>
    <a href="#" id="add_sponsor_image"><?php _e('Add sponsor image','socialsports') ?></a>
    <li class="sponsor_image" id="sponsor_row_placeholder" style="display:none;">
      <p>
        <label><?php _e('Sponsor URL:', 'socialsports') ?></label>
        <input type="text" id="sponsor_url" name="" value="">
      </p>
      <p>
        <img id="sponsor_image" src="" />
        <input type="hidden" id="sponsor_image_url" name="" value="" />
        <span id="image_buttons">
          <input id="upload_button" type="button" class="button" value="<?php _e('Upload Image', 'socialsports') ?>" />
          <span>&nbsp;or&nbsp;</span>
          <input id="choose_from_library_button" type="button" class="button" value="<?php _e('Choose Image from Library', 'socialsports') ?>" />
        </span>
      </p>
      <p><a href="#" class="button"><?php _e('Remove Sponsor','socialsports') ?></a></p>
      <br />
    </li>
  <?php
} // end socialsports_sponsor_images_callback

/* ------------------------------------------------------------------------ *
 * Filters
 * ------------------------------------------------------------------------ */
function socialsports_options_sanitize_and_validate($input)
{
  // $default_options = socialsports_get_default_options();
  // $output = array();
  // $submit = ! empty($input['submit']) ? true : false;
  // $reset = ! empty($input['reset']) ? true : false;
  // if($reset){
  //   $output = $default_options;
  // } elseif($submit) {
  //   $output['sponsor_image'] = $input['sponsor_image'];
  //   $output['twitter'] = $input['twitter'];
  //   $output['facebook'] = $input['facebook'];
  //   $output['meetup'] = $input['meetup'];
  // }
  
  // foreach($input as $key => $val){
  //   if(isset($input[$key])){
  //     $output[$key] = esc_url_raw(strip_tags(stripslashes($input[$key])));
  //   }
  // }
  
  //return apply_filters('socialsports_sanitize_urls', $output, $input);
  return $input;
} // end socialsports_sanitize_url
