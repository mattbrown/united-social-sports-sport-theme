<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

    <div id="content">
        <div id="main-content" class="main-content-page">
            <div class="header-page-content">
                <?php
if(function_exists('bcn_display'))
{
    bcn_display();
}
?>
            </div>
            <div class="page-content">
                
                <?php
    			/* Run the loop to output the page.
    			 * If you want to overload this in a child theme then include a file
    			 * called loop-page.php and that will be used instead.
    			 */
    			get_template_part( 'loop', 'page' );
    			?>
                
            </div>
            <div class="footer-page-content">
                <?php //dynamic_sidebar('tweet-and-like')?>
                <div class="clear"></div> 
            </div>
        </div><!--end #main-content-->
        
        <?php get_sidebar()?>
        
    </div><!--end #content-->

<?php get_footer(); ?>
