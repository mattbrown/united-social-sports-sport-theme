<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
$theme_options = get_option('socialsports_theme_options');
?>
	<div id="footer" class="main">
        <div class="footer-left">
            <?php dynamic_sidebar('weather-widget')?>
        </div>
        
        <div class="footer-right">
            &nbsp;<img src="<?php echo esc_url($theme_options['footer_logo'])?>" />
        </div>
        <div class="footer-midd">
            <span>&copy; Copyright <?php echo date('Y')?> </span><span>DCK Sports LLC </span> <span>All Rights Reserved</span>
            <div class="clear"></div>
            <?php wp_nav_menu( array( 'container_class' => 'menu-footer', 'menu'=>'Footer Menu', 'menu_class' => 'footer-menu' ) ); ?>
        </div>
        
        <div class="clear"></div>
    </div><!--end #footer-->

</div><!--end #container-->

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
</body>
</html>
